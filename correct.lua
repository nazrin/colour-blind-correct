#!/bin/env lua
-- ./correct.lua d 0.5 apo.png apod.png
-- License: MPL-2
-- Requires: Lil https://codeberg.org/nazrin/lil
-- Original code from https://galactic.ink/labs/Color-Vision/Javascript/Color.Vision.Daltonize.js
-- Modified to reduce the intensity instead of adding compensation
local lil = require("lil")

local function clamp(n, ma, mi) return math.min((math.max(n, mi)), ma) end

local correctionType = assert(arg[1], "Need type: 'd' or 'p' or 't'")
local strength = clamp(tonumber(assert(arg[2], "Need strength: 0.0 to 1.0"), 10), 1, 0);
local inPath = assert(arg[3], "Need in path")
local outPath = assert(arg[4], "Need out path")

local img = lil.open(inPath)

local CVDMatrix = {
	d = { -- Deuteranope: greens are greatly reduced (1% men)
		1.0,      0.0, 0.0,
		0.494207, 0.0, 1.24827,
		0.0,      0.0, 1.0
	},
	p = { -- Protanope: reds are greatly reduced (1% men)
		0.0, 2.02344, -2.52581,
		0.0, 1.0,      0.0,
		0.0, 0.0,      1.0
	},
	t = { -- Tritanope: blues are greatly reduced (0.003% population)
		1.0,       0.0,      0.0,
		0.0,       1.0,      0.0,
		-0.395913, 0.801109, 0.0
	}
}

local function filter(img, cvd, strength)
	local istrength = 1 - strength
	local cvd_a = cvd[1]
	local cvd_b = cvd[2]
	local cvd_c = cvd[3]
	local cvd_d = cvd[4]
	local cvd_e = cvd[5]
	local cvd_f = cvd[6]
	local cvd_g = cvd[7]
	local cvd_h = cvd[8]
	local cvd_i = cvd[9]
	local L, M, S, l, m, s, R, G, B, RR, GG, BB
	img:map(function(r, g, b, a)
		L = (17.8824 * r) + (43.5161 * g) + (4.11935 * b)
		M = (3.45565 * r) + (27.1554 * g) + (3.86714 * b)
		S = (0.0299566 * r) + (0.184309 * g) + (1.46709 * b)
		-- Simulate color blindness
		l = (cvd_a * L) + (cvd_b * M) + (cvd_c * S)
		m = (cvd_d * L) + (cvd_e * M) + (cvd_f * S)
		s = (cvd_g * L) + (cvd_h * M) + (cvd_i * S)
		-- LMS to RGB matrix conversion
		R = (0.0809444479 * l) + (-0.130504409 * m) + (0.116721066 * s)
		G = (-0.0102485335 * l) + (0.0540193266 * m) + (-0.113614708 * s)
		B = (-0.000365296938 * l) + (-0.00412161469 * m) + (0.693511405 * s)
		-- Isolate invisible colors to color vision deficiency (calculate error matrix)
		R = r - R
		G = g - G
		B = b - B
		-- Shift colors towards visible spectrum (apply error modifications)
		RR = (0.0 * R) + (0.0 * G) + (0.0 * B)
		GG = (0.7 * R) + (1.0 * G) + (0.0 * B)
		BB = (0.7 * R) + (0.0 * G) + (1.0 * B)
		-- remove compensation from original values
		R = r-RR
		G = g-GG
		B = b-BB
		-- average
		R = (r*istrength + R*strength)
		G = (g*istrength + G*strength)
		B = (b*istrength + B*strength)
		return R, G, B, a
	end)
end

filter(img, assert(CVDMatrix[correctionType], "Invalid type"), strength)

img:save(outPath)

