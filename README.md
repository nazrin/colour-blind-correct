# Colour Blindness Correction

Colour blindness correction on Linux with Picom and images with Lua

Original code from https://galactic.ink/labs/Color-Vision/Javascript/Color.Vision.Daltonize.js

Modified to reduce the intensity instead of adding compensation

## Lua version:

Requires: Lil https://codeberg.org/nazrin/lil

```sh
./correct.lua d 0.5 apo.png apod.png
```

## Picom GLSL shader:

You can edit the params inside the file

```sh
picom --backend glx --glx-fshader-win "$(cat correct.frag)"
```

## License

MPL-2


