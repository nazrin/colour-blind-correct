#version 120
// picom --backend glx --glx-fshader-win "$(cat correct.frag)"
// License: MPL-2
// Original code from https://galactic.ink/labs/Color-Vision/Javascript/Color.Vision.Daltonize.js
// Modified to reduce the intensity instead of adding compensation

const float strength = 0.5;

// Uncomment the type that affects you
const float cvd[9] =
	float[9]( // Deuteranope: greens are greatly reduced 1% men
		1.0,      0.0, 0.0,
		0.494207, 0.0, 1.24827,
		0.0,      0.0, 1.0
	);
	/* float[9]( // Protanope: reds are greatly reduced (1% men) */
	/* 	0.0, 2.02344, -2.52581, */
	/* 	0.0, 1.0,      0.0, */
	/* 	0.0, 0.0,      1.0 */
	/* ); */
	/* float[9]( // Tritanope: blues are greatly reduced (0.003% population) */
	/* 	1.0,       0.0,      0.0, */
	/* 	0.0,       1.0,      0.0, */
	/* 	-0.395913, 0.801109, 0.0 */
	/* ); */

uniform sampler2D tex;
uniform float opacity;

const float cvd_a = cvd[0];
const float cvd_b = cvd[1];
const float cvd_c = cvd[2];
const float cvd_d = cvd[3];
const float cvd_e = cvd[4];
const float cvd_f = cvd[5];
const float cvd_g = cvd[6];
const float cvd_h = cvd[7];
const float cvd_i = cvd[8];

const float istrength = 1.0 - strength;

void main() {
	vec4 color = texture2D(tex, gl_TexCoord[0].xy);
	float r = color.r;
	float g = color.g;
	float b = color.b;
	float L, M, S, l, m, s, R, G, B, RR, GG, BB;
	L = (17.8824 * r) + (43.5161 * g) + (4.11935 * b);
	M = (3.45565 * r) + (27.1554 * g) + (3.86714 * b);
	S = (0.0299566 * r) + (0.184309 * g) + (1.46709 * b);
	// Simulate color blindness
	l = (cvd_a * L) + (cvd_b * M) + (cvd_c * S);
	m = (cvd_d * L) + (cvd_e * M) + (cvd_f * S);
	s = (cvd_g * L) + (cvd_h * M) + (cvd_i * S);
	// LMS to RGB matrix conversion
	R = (0.0809444479 * l) + (-0.130504409 * m) + (0.116721066 * s);
	G = (-0.0102485335 * l) + (0.0540193266 * m) + (-0.113614708 * s);
	B = (-0.000365296938 * l) + (-0.00412161469 * m) + (0.693511405 * s);
	// Isolate invisible colors to color vision deficiency (calculate error matrix)
	R = r - R;
	G = g - G;
	B = b - B;
	// Shift colors towards visible spectrum (apply error modifications)
	RR = (0.0 * R) + (0.0 * G) + (0.0 * B);
	GG = (0.7 * R) + (1.0 * G) + (0.0 * B);
	BB = (0.7 * R) + (0.0 * G) + (1.0 * B);
	// remove compensation from original values
	R = r - RR; // R = RR + r;
	G = g - GG; // G = GG + g;
	B = b - BB; // R = BB + b;
	// change strength
	R = (r*istrength + R*strength);
	G = (g*istrength + G*strength);
	B = (b*istrength + B*strength);
	gl_FragColor = vec4(R, G, B, color.a * opacity);
}

